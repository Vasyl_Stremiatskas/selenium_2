package epam.lab.selenium_2;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class TestOne {
    private static WebDriver driver;

    // test data
    private String testEmail_1 = "vasua1975@gmail.com";
    private String testPass_1 = "1111dfcz";
    private String testEmail_2 = "vasua.vasua1999@gmail.com";

    @BeforeClass
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", "\\JavaProjects\\Selenium_2.0\\WebDrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.google.com/intl/ru/gmail/about/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void UserLoginAndSendEmail() {
        WebElement loginBtn = driver.findElement(By.xpath("/html/body/nav/div/a[2]"));
        loginBtn.click();

        WebElement loginForm = driver.findElement(By.xpath("//*[@id=\"identifierId\"]"));
        loginForm.sendKeys(testEmail_1);
        loginForm.sendKeys(Keys.ENTER);

        WebElement passwordForm  = driver.findElement(By.xpath("//*[@id=\"password\"]/div[1]/div/div[1]/input"));
        passwordForm.sendKeys(testPass_1);
        passwordForm.sendKeys(Keys.ENTER);

        WebElement writeEmailBtn = driver.findElement(By.xpath("//*[@id=\":h5\"]/div/div"));
        writeEmailBtn.click();

        WebElement forWhom = driver.findElement(By.xpath("//*[@id=\":nb\"]"));
        forWhom.sendKeys( testEmail_2);
        WebElement emailTitle = driver.findElement(By.xpath("//*[@id=\":mt\"]"));
        emailTitle.sendKeys("hi from selenium");
        WebElement sendEmailBtn = driver.findElement(By.xpath("//*[@id=\":mj\"]"));
        sendEmailBtn.click();
    }

    @AfterClass
    public static void shutDown() {
        driver.close();
    }
}
